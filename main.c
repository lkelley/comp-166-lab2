#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/**
 * Reads a temperature in Celsius, Kelvin, or Fahrenheit and outputs the equivalent in the other two units
 */
int main() {
    double degC;
    double degF;
    double degK;

    char cInputTemp[256];
    double dInput;
    char *endPtr;
    char cInputUnits;

    /*
     * Prompt for temperature, read into "input"
     */
    printf("Enter a temperature followed by F, C, or K. e.g., 24.5F: ");
    fgets(cInputTemp, sizeof(cInputTemp), stdin);

    // Convert char input to double
    dInput = strtod(cInputTemp, &endPtr); // endPtr stores remaining string

    // Get unit char
    cInputUnits = endPtr[0];

    /*
     * Store input temp as Celsius, converting as needed
     */
    switch (toupper(cInputUnits)) {
        case 'C':
            degC = dInput;
            break;

        case 'F':
            degC = (dInput - 32.0) / 1.8;
            break;

        case 'K':
            degC = dInput - 273.15;
            break;

        default:
            printf("Unknown temperature type: %s", endPtr);
            return EXIT_FAILURE;
    }

    /*
     * Convert from Celsius to other units
     */
    degK = degC + 273.15;
    degF = degC * 1.8 + 32;

    /*
     * Print out temperatures
     */
    printf("%.3fK = %.3fC = %.3fF\n", degK, degC, degF);

    return EXIT_SUCCESS;
}
